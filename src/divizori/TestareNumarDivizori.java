package divizori;

import divizori.Numere;

public class TestareNumarDivizori{
	
	public static void main (String[] args)
	{
		Numere numar1 = new Numere();
		Numere numar2 = new Numere();
		Numere numar = new Numere();
		
		System.out.println("Va rugam introduceti primul numar:");
		numar.creare(numar1);
		
		System.out.println("Va rugam introduceti al doilea numar:");
		numar.creare(numar2);
		
		if(numar1.divizori == numar2.divizori)
			System.out.println("Cele doua numere au acelasi numar de divizori.");
		else
			System.out.println("Cele doua numere nu au acelasi numar de divizori.");
	}
}
