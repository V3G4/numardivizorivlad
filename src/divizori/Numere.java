package divizori;

import java.util.Scanner;

public class Numere {
	int numar;
	int divizori;
	//clasa noastra va retine valoarea numarului in variabila numar, respectiv numarul de divizori in variabila divizori
	
	Scanner scan = new Scanner(System.in);
	
	public void creare(Numere num)
	{
		while(!scan.hasNextInt())
		{
			System.out.println("Date incorecte. Va rugam reintroduceti:");
			//prevenim iesirea fortata din program in cazul datelor incorecte introduse
			scan.next();
		}
		num.numar = scan.nextInt();
		
		num.divizori = numarDivizori(num.numar);
	}
	
	private int numarDivizori(int num)
	{
		int divizor;
		
		if(numar == 0)
			return numar;
		
		if(numar < 0)
			numar *= -1;
		
		for(divizor = 1; divizor <= numar/2; ++divizor)
			if(numar % divizor == 0)
				++divizori;
		
		return divizori;
	}
}
